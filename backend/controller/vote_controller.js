const {db, build_result} = require('../config/config')

module.exports = [
    {
        method: 'get',
        path: '/getVoteInfo',
        action: async function (ctx, next) {
            if (!ctx.request.query.voteId) {
                ctx.body = build_result(-1, '参数错误');
                return;
            }
            let result = await db.mysql.query_f(db.mysql.query_str.query_vote_info, [ctx.request.query.voteId]);
            ctx.body = build_result(0, result);
        }
    },
    {
        method: 'get',
        path: '/getCandidateList',
        action: async function (ctx, next) {
            if (!ctx.request.query.voteId) {
                ctx.body = build_result(-1, '参数错误');
                return;
            }
            let result = await db.mysql.query_f(db.mysql.query_str.query_candidate_list, [ctx.request.query.voteId]);
            let results = [];
            for (let i = 0; i < result.results.length; i++) {
                let tmp = result.results[i];
                results.push({
                    id: tmp.id,
                    description: tmp.description,
                    name: tmp.name,
                    vote_id: tmp.vote_id,
                    votes: tmp.votes
                })
            }

            results.sort(function (a, b) {
                return b.votes - a.votes;
            });
            ctx.body = build_result(0, results);
        }
    },
    {
        method: 'get',
        path: '/getCandidateDetail',
        action: async function (ctx, next) {
            if (!ctx.request.query.candidateId) {
                ctx.body = build_result(-1, '参数错误');
                return;
            }
            let result = await db.mysql.query_f(db.mysql.query_str.query_candidate_detail, [ctx.request.query.candidateId]);
            ctx.body = build_result(0, result);
        }
    },
    {
        method: 'post',
        path: '/addVote',
        action: async function (ctx, next) {
            if (!ctx.request.body.candidateId) {
                ctx.body = build_result(-1, '参数错误');
                return;
            }
            let result = await db.mysql.query_f(db.mysql.query_str.add_votes, [ctx.request.body.candidateId]);
            ctx.body = build_result(0, result);
        }
    }
]
