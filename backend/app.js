const Koa = require('koa2');
const cors = require('koa2-cors');
const staticResource = require('koa-static')
const koaBody = require('koa-body');
const mysql = require('mysql')
const {origin_config,db} = require('./config/config');
const router = require('./routes/index');
const app = new Koa();
db.mysql.pool = db.mysql.create_pool(mysql);

app.use(staticResource(__dirname + '/www'));
app.use(cors(origin_config));
app.use(koaBody());
app.use(router.routes());

app.listen(3000);