let Router = require('koa-router')
let fs = require('fs');
let router = new Router();

const controller_dir = 'controller'


function isArrayFn(value) {
    if (typeof Array.isArray === "function") {
        return Array.isArray(value);
    } else {
        return Object.prototype.toString.call(value) === "[object Array]";
    }
}

function string_end_with(str, end_with) {
    let d = str.length - end_with.length;
    return (d >= 0 && str.lastIndexOf(end_with) === d);
}

function router_action(controller) {
    router[controller.method](controller.path, controller.action)
}

fs.readdir(controller_dir, ((err, files) => {
    if (err) {
        console.error(err);
        return;
    }
    files.forEach((file, _) => {
        if (!string_end_with(file, 'controller.js')) return;
        let router = require(`../controller/${file}`);
        if (isArrayFn(router)) {
            for (let i = 0; i < router.length; i++) {
                let tmp = router[i];
                router_action(tmp)
            }
        } else {
            router_action(router)
        }
    })
}))

module.exports = router;