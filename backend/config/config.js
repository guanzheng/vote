module.exports = {
    origin_config: {
        origin: (ctx) => {
            if (ctx.url === '/test') {
                return "*"; // 允许来自所有域名请求
            }
            return 'http://localhost:8080'; // 这样就能只允许 http://localhost:8080 这个域名的请求了
        },
        exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
        maxAge: 5,
        credentials: true,
        allowMethods: ['GET', 'POST', 'DELETE'],
        allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
    },
    db: {
        mysql: {
            options: {
                host: '127.0.0.1',
                user: 'root',
                password: '123456',
                database: 'vote'
            },
            create_pool: function (mysql) {
                return mysql.createPool({
                    connectionLimit: 10,
                    host: this.options.host,
                    user: this.options.user,
                    password: this.options.password,
                    database: this.options.database
                })
            },
            pool: undefined,
            query_f: function (query_str, query_parameters) {
                return new Promise((resolve, reject) => {
                    this.pool.query(query_str, query_parameters, (error, results, fields) => {
                        if (error) {
                            reject(error);
                            return;
                        }
                        resolve({results, fields})
                    })
                })
            },
            query_str: {
                query_vote_info: `SELECT id,description,title FROM t_vote WHERE id = ?`,
                query_candidate_list: `SELECT id,vote_id,name,votes,description FROM t_candidate WHERE vote_id = ?`,
                query_candidate_detail: `SELECT id,vote_id,name,votes,description,photo FROM t_candidate WHERE id = ?`,
                add_votes: `UPDATE t_candidate SET votes = votes + 1 WHERE id = ?`
            }
        }
    },
    build_result: function (code, data) {
        return {code, data}
    }
}
