import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/Home.vue'
import Login from '../pages/home/Login.vue'
import GoodsDetail from '../pages/GoodsDetail/GoodsDetail.vue'
import Banner from '../pages/Banner/Banner.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children:[
        {
          path:'/login',
          component:Login
        },
        {
          path:'/list',
          component:Banner
        },
        {
          path:'/detail',
          component:GoodsDetail
        }
      ]
    }
  ]
})
